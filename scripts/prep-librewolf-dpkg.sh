#!/bin/sh
# File: prep-librewolf-dpkg.sh
# Location: https://gitlab.com/bgstack15/librewolf-linux.git
# Latest supported version: librewolf-85.0-1
# Author: bgstack15
# SPDX-License-Identifier: CC-BY-SA-4.0
# Startdate: 2020-11-29
# Title: Build Dpkg for LibreWolf
# Purpose: Prepare initial assets for running "dpkg-buildpackage -b -us -uc" for LibreWolf by adapting distro Firefox assets
# History:
#    2021-03-10 add initial Ubuntu support
# Usage:
#    Can send these final assets up to Open Build Service
# References:
#    Script numbers from https://gitlab.com/librewolf-community/browser/linux/-/tree/master/binary_tarball/scripts
# Improve:
#    Make this idempotent. Right now it is very much not.
# Dependencies:
#    wget, git, tar, awk, sed

# Aborts the script upon any faliure
set -e;

#####################################
# Load settings
# basically, dot-source the conf file.
test -z "${librewolf_dpkg_conf}" && export librewolf_dpkg_conf="$( find "$( dirname "${0}" )" -maxdepth 2 -name "$( basename "${0%%.sh}.conf" )" -print 2>/dev/null | head -n1 )"
test ! -r "${librewolf_dpkg_conf}" && { echo "Unable to load config file, which should be named the same as this script but with a .conf ending. Aborted." 1>&2 ; exit 1 ; }
. "${librewolf_dpkg_conf}"

librewolf_common_url=https://gitlab.com/librewolf-community/browser/common.git
librewolf_settings_url=https://gitlab.com/librewolf-community/settings.git
librewolf_linux_url=https://gitlab.com/librewolf-community/browser/linux.git

_mozconfig='/config/mozconfig.in'

# user configurable
git_source_dir=${CI_PROJECT_DIR}/git  # where LibreWolf git contents are cached
debian_dir=${CI_PROJECT_DIR}/${firefox_version}/debian # where the firefox_debian.tar.xz file is extracted
source_dir=${CI_PROJECT_DIR}/${firefox_version}/librewolf_${firefox_version} # where firefox.orig.tar.xz file is extracted with --strip-components=1
work_dir=${CI_PROJECT_DIR}/prepared/

#####################################
# Download initial components

test -z "${SKIP_APT}" && apt update && apt install -y git curl wget xz-utils

# Download upstream distro assets, which includes
#   1. orig tarball, which in Debian is not always the pristine contents from upstream source
#   2. debian/ directory which defines how to build a dpkg
#   3. source package control file
mkdir -p "${work_dir}" ; cd "${work_dir}"
test -z "${SKIP_DOWNLOAD}" && {
  wget --content-disposition http://archive.ubuntu.com/ubuntu/pool/main/f/firefox/firefox_"${firefox_version}".orig.tar.xz
  wget --content-disposition http://archive.ubuntu.com/ubuntu/pool/main/f/firefox/firefox_"${distro_firefox_version}".debian.tar.xz
  wget --content-disposition http://archive.ubuntu.com/ubuntu/pool/main/f/firefox/firefox_"${distro_firefox_version}".dsc
}

# extract these contents to where they belong
mkdir -p "${source_dir}"
test -z "${SKIP_EXTRACT}" && {
   echo "Extracting files from orig and debian tarballs. This might take a while." 1>&2
   tar -C "${source_dir}" -Jx --strip-components=1 -f firefox_"${firefox_version}".orig.tar.xz
   tar -C "$( dirname "${debian_dir}" )" -Jxf firefox_"${distro_firefox_version}".debian.tar.xz
   # dsc file is a text file and needs no extraction
}

# Download git sources
# test -z "${SKIP_GIT}" && (
   # yes, use a sub-shell because of this cd. pushd is a bash builtin, but we are using sh and not bash.
   # cd "${git_source_dir}"
   git clone "${librewolf_common_url}" ${git_source_dir}/common || ( cd "${git_source_dir}/common" ; git checkout master ; git pull ; )
   git clone "${librewolf_settings_url}" ${git_source_dir}/settings || ( cd "${git_source_dir}/settings" ; git checkout master ; git pull ; )
   git clone "${librewolf_linux_url}" ${git_source_dir}/linux || ( cd "${git_source_dir}/linux" ; git checkout master ; git pull ; )

# )

#####################################
# Script 1 tasks

# update debian/control file
# update fields and add libjack-dev
sed -i -r "${debian_dir}"/control \
   -e '/^[[:alpha:]]+: firefox/s/firefox/librewolf/' \
   -e '/^Package:.*-dbg/,$d' \
   -e '/^Maintainer:/{s/Maintainer:/XSBC-Original-Maintainer:/;iMaintainer: B. Stack <bgstack15@gmail.com>' -e '}' \
   -e '/^Uploaders:/d' \
   -e '/libasound2-dev/s/libasound2-dev,/libasound2-dev, libjack-dev,/;' \
   -e '/^Vcs-/d' \
   -e '/Breaks:.*xul-ext-torbutton/d' \
   -e '/Description:/,+8{/Description:/,/^\s*$/d}'
cat <<'EOF' >> "${debian_dir}"/control
Description: LibreWolf variant of Mozilla Firefox web browser
 LibreWolf is a build of Firefox that seeks to protect user privacy,
 security, and freedom.
EOF

# we remove those packages/build steps with the sed above, so let's remove references to it
# so packaging won't fail.
sed -i '/geckodriver\|dbg-package/d' "${debian_dir}"/build/rules.mk
sed -i '/minidump-analyzer\|crashreporter\|Throbber/d' "${debian_dir}"/firefox.install.in

# fix naming (used in the .desktop file, for example)
sed -i -e 's/\(MOZ_DISPLAY_NAME = \).*/\1LibreWolf/' \
   -e '/brandShortName/d' "${debian_dir}"/build/rules.mk

#####################################
# Script 2 tasks

# none. Dependencies are handled by the build environment by interpreting the dsc file.

#####################################
# Script 3 tasks

# overlay the orig tarball contents with LibreWolf contents
# LibreWolf branding
cp -pr "${git_source_dir}"/common/source_files/browser/branding "${source_dir}"/browser/
# update mozconfig with needed info
sed -i -e '/with-app-name=/d' "${debian_dir}${_mozconfig}"
cat <<EOF >> "${debian_dir}${_mozconfig}"

# Start of LibreWolf effects
ac_add_options --disable-tests
ac_add_options --disable-debug

ac_add_options --prefix=/usr
ac_add_options --enable-release
ac_add_options --enable-hardening
ac_add_options --enable-rust-simd

# Branding ac_add_options --enable-update-channel=release
ac_add_options --with-app-name=librewolf
ac_add_options --with-app-basename=LibreWolf
ac_add_options --with-branding=browser/branding/librewolf
ac_add_options --with-distribution-id=io.gitlab.librewolf
ac_add_options --with-unsigned-addon-scopes=app,system
ac_add_options --allow-addon-sideload
export MOZ_REQUIRE_SIGNING=0

# Features
ac_add_options --enable-jack
ac_add_options --disable-crashreporter

# Disables crash reporting, telemetry and other data gathering tools
mk_add_options MOZ_CRASHREPORTER=0
mk_add_options MOZ_DATA_REPORTING=0
mk_add_options MOZ_SERVICES_HEALTHREPORT=0
mk_add_options MOZ_TELEMETRY_REPORTING=0

EOF
test "$( uname -p )" = "x86_64" && { echo "ac_add_options --disable-elf-hack" >> "${debian_dir}${_mozconfig}" ; }
cat <<EOF >> "${debian_dir}${_mozconfig}"
# LibreWolf binary release uses clang-11 but Debian builds Firefox with gcc so this is irrelevant.
#export CC='clang-11'
#export CXX='clang++-11'
#export AR=llvm-ar-11
#export NM=llvm-nm-11
#export RANLIB=llvm-ranlib-11

ac_add_options --enable-optimize
EOF

# workaround to allow an older cargo version until 0.48 is available in SID
sed -i 's/cargo (>= 0.47),/cargo (>= 0.46),/g' "${debian_dir}"/control.in
sed -i 's/cargo (>= 0.47),/cargo (>= 0.46),/g' "${debian_dir}"/control
sed -i 's/rustc (>= 1.47),/rustc (>= 1.41),/g' "${debian_dir}"/control
sed -i 's/rustc (>= 1.47),/rustc (>= 1.41),/g' "${debian_dir}"/control.in

# add patches to debian/patches
mkdir -p "${debian_dir}"/patches/librewolf

cp -pr "${git_source_dir}"/common/patches/megabar.patch \
  "${git_source_dir}"/common/patches/remove_addons.patch \
  "${git_source_dir}"/common/patches/mozilla-vpn-ad.patch \
  "${git_source_dir}"/common/patches/sed-patches/disable-pocket.patch \
  "${git_source_dir}"/common/patches/context-menu.patch \
  "${git_source_dir}"/common/patches/browser-confvars.patch \
  "${git_source_dir}"/common/patches/urlbarprovider-interventions.patch \
  "${git_source_dir}"/common/patches/sed-patches/remove-internal-plugin-certs.patch \
  "${git_source_dir}"/common/patches/sed-patches/allow-searchengines-non-esr.patch \
  "${git_source_dir}"/common/patches/search-config.patch \
  "${git_source_dir}"/common/patches/sed-patches/stop-undesired-requests.patch \
  "${git_source_dir}"/common/patches/allow_dark_preference_with_rfp.patch \
  "${git_source_dir}"/common/patches/about-dialog.patch \
  "${debian_dir}"/patches/librewolf/
cat <<EOF >> "${debian_dir}"/patches/series
librewolf/remove_addons.patch -p1
librewolf/megabar.patch -p1
librewolf/mozilla-vpn-ad.patch -p1
librewolf/disable-pocket.patch -p1
librewolf/context-menu.patch -p1
librewolf/browser-confvars.patch -p1
librewolf/urlbarprovider-interventions.patch -p1
librewolf/remove-internal-plugin-certs.patch -p1
librewolf/allow-searchengines-non-esr.patch -p1
librewolf/search-config.patch -p1
librewolf/stop-undesired-requests.patch -p1
librewolf/allow_dark_preference_with_rfp.patch -p1
librewolf/about-dialog.patch -p1
EOF

#####################################
# Script 4 tasks

# Additional steps for dpkg implementation

# fix the binary name that gets installed in /usr/bin, and disable crash reporter by changing what variable name it looks for that will enable it

export MOZ_ENABLE_BREAKPAD=0
export MOZ_APP_NAME=librewolf

# instruct dpkg to include the librewolf settings
rm -rf "${debian_dir}"/librewolf_settings
cp -pr "${git_source_dir}"/settings "${debian_dir}"/librewolf_settings
rm -rf "${debian_dir}"/librewolf_settings/.git*

cat <<EOF >> "${debian_dir}"/firefox.install.in

debian/librewolf_settings/librewolf.cfg @MOZ_LIBDIR@/
debian/librewolf_settings/defaults/pref/local-settings.js @MOZ_LIBDIR@/defaults/pref/
debian/librewolf_settings/distribution/policies.json @MOZ_LIBDIR@/distribution/
EOF

# add changelog contents for LibreWolf
new_changelog="$( mktemp )"
{
   cat <<EOF
librewolf (${distro_firefox_version}) unstable; urgency=low

  * Fork to librewolf release

 -- B. Stack <bgstack15@gmail.com>  $( date "+%a, %d %b %Y %T %z" )

EOF
   cat "${debian_dir}"/changelog
} > "${new_changelog}"
cat "${new_changelog}" > "${debian_dir}"/changelog

rm -f "${new_changelog:-NOTHINGTODEL}"

# remove dpkg-divert items which librewolf will not use
# thankfully dpkg-divert is the only tasks in these files as of 85.0
rm -f "${debian_dir}"/browser.postrm.in "${debian_dir}"/browser.preinst.in

#####################################
# Build new assets
# dpkg-buildpackage needs the orig tarball, debian tarball, and dsc file.

echo "Building new tarballs. This might take a while." 1>&2

# orig tarball
cd "${work_dir}"
tar -Jc -f librewolf_"${firefox_version}".orig.tar.xz -C "$( dirname "${source_dir}" )" librewolf_"${firefox_version}"

# debian tarball
tar -Jc -f librewolf_"${distro_firefox_version}".debian.tar.xz -C "$( dirname  "${debian_dir}" )" debian

# dsc file, which needs to be modified
cd "${work_dir}"
sed -r \
   -e '/^(Files|Checksums-.{0,8}):/,$d' \
   -e '1,/^Format:/{/^Format:/!{d}}' \
   -e 's/^([[:alpha:]]+:).* firefox(-l10n[^\s]*)*/\1 librewolf/' \
   -e '/firefox-dbg/d' \
   -e '/^Maintainer:/{s/Maintainer:/XSBC-Original-Maintainer:/;iMaintainer: B. Stack <bgstack15@gmail.com>' -e '}' \
   -e '/^Uploaders:/d' \
   -e '/libasound2-dev/s/libasound2-dev,/libasound2-dev, libjack-dev,/;' \
   -e '/^Vcs-/d' \
   -e '/^ firefox/s/firefox/librewolf/g' \
   -e 's/cargo \(>= 0\.47\),/cargo \(>= 0\.46\),/' \
   -e 's/rustc \(>= 1\.47\),/rustc \(>= 1\.41\),/' \
   firefox_"${distro_firefox_version}".dsc > librewolf_"${distro_firefox_version}".dsc
{
   echo "Files:"
   for word in librewolf*z ;
   do
      printf "%s %s\n" "$( stat -c '%s' "${word}" )" "$( md5sum "${word}" )"
   done | awk '{print " "$2,$1,$3}'
} >> librewolf_"${distro_firefox_version}".dsc

# And now you have in the ${work_dir} location three files.
# librewolf_80.3.orig.tar.xz librewolf_80.3-1.debian.tar.xz librewolf_80.3-1.dsc
